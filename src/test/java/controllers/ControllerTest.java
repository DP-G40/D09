
package controllers;

import controllers.administrator.CommentAdministratorController;
import controllers.user.CommentUserController;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@WebAppConfiguration
public class ControllerTest extends AbstractTest{

	private MockMvc				mockMvc;

	@InjectMocks
	private CommentAdministratorController commentController;


	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.commentController).build();
	}

	@Test
	public void testGetPerson() throws Exception {
		authenticate("admin");
		this.mockMvc.perform(MockMvcRequestBuilders.get("/comment/administrator/delete.do?commentId=55")).andExpect(MockMvcResultMatchers.status().isOk());
		unauthenticate();

	}

	@Test
	public void testGetPersonError() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.get("/comment/delete?commentId=")).andExpect(MockMvcResultMatchers.status().isNotFound());

	}
	@Test
	public void testGetPersonError2() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.get("/commDPent/admin/delete?commentId=")).andExpect(MockMvcResultMatchers.status().isNotFound());

	}
}
