package services;

import java.util.Date;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.User;
import forms.RegisterForm;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class UserServiceTest extends AbstractTest {

	@Autowired
	private UserService userService;

	@Test
	public void saveAndDeleteDriver() {

		final Object testingData[][] = {
				{
					"surname1","name1", "surname1", "email1@email.com", "666666666", "direccion1","password1","password1",new Date(), false ,null
				},{
					"surname2","name2", "surname2", "email1@email.com", "666666666", "direccion1","password2","password2",new Date() ,false,null
				},{
					"surname3","name3", "surname3", "email1@email.com", "666666666", "direccion1", "password3","password3",new Date(),true, null
				},{
					null ,null, "surname4",  "email1@email.com", "666666666", "direccion1","password","password", new Date(), false, ConstraintViolationException.class
				}
			};
				
			
			
		for (int i = 0; i< testingData.length; i++){
			this.ReconstructSaveAndDeleteTemplate((String) testingData[i][0], 
			(String) testingData[i][1],(String) testingData[i][2], (String) testingData[i][3],
			(String) testingData[i][4],(String) testingData[i][5],(String) testingData[i][6],(String) testingData[i][7], (boolean) testingData[i][9],(Date) testingData[i][8],(Class<?>) testingData[i][10]); 
		}	
			
		

	}

	 protected void ReconstructSaveAndDeleteTemplate(final String username,final String name, final String surname,
			 final String email, final String phonenumber, final String direction, 
			 final String password, final String repeatPassword,final boolean check,final Date birthday ,final Class<?> expected) { 
    Class<?> caught; 
 
    caught = null; 
    try { 
      
      RegisterForm userForm = new RegisterForm();
      userForm.setUserName(username);
      userForm.setName(name);
      userForm.setSurname(surname);
      userForm.setAddress(direction);
      userForm.setPhone(phonenumber);
      userForm.setEmail(email);
      userForm.setPassword(password);
      userForm.setRepeatPassword(repeatPassword);
      userForm.setCheck(check);
      userForm.setBirthday(birthday);
     
     
       
      User user = userService.reconstruct(userForm); 
       
      user = userService.flush(user);
      userService.delete(user); 
      Assert.isTrue(userService.findOne(user.getId())==null);
       
    } catch (final Throwable oops) {
      caught = oops.getClass(); 
    } 
 
    this.checkExceptions(expected, caught); 
  } 
	
}
