package services;

import domain.CreditCard;
import domain.Rendezvous;
import domain.Request;
import domain.Service;
import forms.RequestForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utilities.AbstractTest;

import javax.transaction.Transactional;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class RequestServiceTest extends AbstractTest{

    @Autowired
    private RequestService requestService;

    @Autowired
    private RendezvousService rendezvousService;

    @Autowired
    private ServiceService serviceService;


    public void createRequestTest(String user ,Integer rendezvousId , Integer serviceId, String holder,String brand,String number,String cvv,String month,String year,Class<?> expected){
        authenticate(user);
        Class<?> caught = null;
        try {
            RequestForm requestForm = new RequestForm();
            CreditCard creditCard = new CreditCard(brand, holder, number, cvv, month, year);
            Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
            Service service = serviceService.findOne(serviceId);
            requestForm.setCreditCard(creditCard);
            requestForm.setRendezvous(rendezvous);
            requestForm.setService(service);
            Request request = requestService.reconstruct(requestForm);
            requestService.save(request);
        } catch (Throwable oops) {
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);

    }

    @Test
    public void createRequesDriver() {
        final Object testingData[][] = {
                {
                        "user1", 50, 70, "holder", "brand", "376592094004746", "100", "10", "2018", null
                }, {
                "admin", 50, 70, "holder", "brand", "376592094004746", "100", "10", "2018", ClassCastException.class
        }
        };

        for (int i = 0; i < testingData.length; i++) {
            this.createRequestTest((String) testingData[i][0], ((Integer) testingData[i][1]),((Integer) testingData[i][2]),((String) testingData[i][3]),((String) testingData[i][4]),((String) testingData[i][5]),
                    ((String) testingData[i][6]),((String) testingData[i][7]),((String) testingData[i][8]), (Class<?>) testingData[i][9]);
        }
    }
}
