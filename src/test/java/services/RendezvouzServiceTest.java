/*
 * SampleTest.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Comment;
import domain.Configuration;
import domain.Rendezvous;
import forms.CommentForm;
import forms.ConfigurationForm;
import forms.RendezvousForm;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class RendezvouzServiceTest extends AbstractTest {

	// System under test ------------------------------------------------------

	// Tests ------------------------------------------------------------------

	// The following are fictitious test cases that are intended to check that 
	// JUnit works well in this project.  Just righ-click this class and run 
	// it using JUnit.
	
	@Autowired
    private RendezvousService rendezvousService;
	
	@Autowired
    private UserService userService;

	@Test
	public void constructReconstructDriver() {
		final Object testingData[][] = {
			{
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name1","Text1", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name2", "Text2", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name3", "Text3", null
			}, {
				null, "Name4", "Text4", null
			}, {
				null, null, null, null
			}, {
				null, null, "Text6", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name7", "Text7", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name8", "Text8", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name9", "Text9", null
			}, {
				null, "Name10", null, null
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.constructReconstructAndSaveTemplate((String) testingData[i][0], ((String) testingData[i][1]), ((String) testingData[i][2]), (Class<?>) testingData[i][3]);
	}

	// Ancillary methods ------------------------------------------------------

	protected void constructReconstructAndSaveTemplate(final String picture, final String name, final String description, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(new Date()); 
		    calendar.add(Calendar.DAY_OF_YEAR, 1);  
			
			RendezvousForm rendezvouzForm = new RendezvousForm();
			rendezvouzForm.setName(name);
			rendezvouzForm.setDescription(description);
			rendezvouzForm.setMoment(calendar.getTime());
			rendezvouzForm.setPicture(picture);
			rendezvouzForm.setCoordinateY(12.12);
			rendezvouzForm.setCoordinateX(13.13);
			
			Rendezvous rendezvous = rendezvousService.reconstruct(rendezvouzForm);
			RendezvousForm rendezvouzFormConstruct = rendezvousService.construct(rendezvous);
			Assert.isTrue(rendezvouzFormConstruct!=null);
			
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	
	@Test
	public void createDeleteAndSaveDriver() {
		final Object testingData[][] = {
			{
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name1", "Text1", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name2", "Text2", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Name3", "Text3", null
			}, {
				null, null, null, ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.createDeleteAndSaveTemplate((String) testingData[i][0], ((String) testingData[i][1]), ((String) testingData[i][2]), (Class<?>) testingData[i][3]);
	}

	// Ancillary methods ------------------------------------------------------

	protected void createDeleteAndSaveTemplate(final String picture, final String name, final String description, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(new Date()); 
		    calendar.add(Calendar.DAY_OF_YEAR, 1); 
			
			Rendezvous rendezvous = rendezvousService.create();
			rendezvous.setAuthor(userService.findAll().iterator().next());
			rendezvous.setCoordinateY(12.12);
			rendezvous.setCoordinateX(13.13);
			rendezvous.setDescription(description);
			rendezvous.setMoment(calendar.getTime());
			rendezvous.setName(name);
			rendezvous.setPicture(picture);
			Rendezvous rendezvousSaved = rendezvousService.saveAndFlush(rendezvous);
			rendezvousService.delete(rendezvousSaved);
			//Assert.isTrue(rendezvousService.findOne(rendezvousSaved.getId())==null);
			
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}