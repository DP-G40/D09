/*
 * SampleTest.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import domain.Announcement;
import domain.Comment;
import domain.Configuration;
import forms.AnnouncementForm;
import forms.CommentForm;
import forms.ConfigurationForm;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AnnouncementServiceTest extends AbstractTest {

	// System under test ------------------------------------------------------

	// Tests ------------------------------------------------------------------

	// The following are fictitious test cases that are intended to check that 
	// JUnit works well in this project.  Just righ-click this class and run 
	// it using JUnit.
	
	@Autowired
    private AnnouncementService announcementService;
	
	@Autowired
    private RendezvousService rendezvousService;
	
	@Autowired
    private UserService userService;

	@Test
	public void reconstructSaveAndDeleteDriver() {
		final Object testingData[][] = {
			{
				//Caso Positivo
				"Title1", "Text1", null
			},  {
				//Caso negativo: Titulo a null
				null, "Text4", ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.reconstructSaveAndDeleteTemplate((String) testingData[i][0], ((String) testingData[i][1]), (Class<?>) testingData[i][2]);
	}

	//Test para probar la creacion, edicion, guardado y borrado de un Announcement.
	protected void reconstructSaveAndDeleteTemplate(final String title, final String description, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate("user1");
			AnnouncementForm announcementForm = new AnnouncementForm();
			announcementForm.setDescription(description);
			announcementForm.setMoment(new Date());
			announcementForm.setTitle(title);
			
			BindingResult errors = new BeanPropertyBindingResult(announcementForm, "announcementForm");
			
			Announcement announcement = announcementService.reconstruct(announcementForm, rendezvousService.findAll().iterator().next().getId(), errors);
			
			Announcement announcementSaved = announcementService.saveAndFlush(announcement);
			announcementService.delete(announcementSaved);
			Assert.isTrue(announcementService.findOne(announcementSaved.getId())==null);
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
			//oops.printStackTrace();
		}

		this.checkExceptions(expected, caught);
	}

}