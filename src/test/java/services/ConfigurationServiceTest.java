/*
 * SampleTest.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Comment;
import domain.Configuration;
import forms.CommentForm;
import forms.ConfigurationForm;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ConfigurationServiceTest extends AbstractTest {

	// System under test ------------------------------------------------------

	// Tests ------------------------------------------------------------------

	// The following are fictitious test cases that are intended to check that 
	// JUnit works well in this project.  Just righ-click this class and run 
	// it using JUnit.
	
	@Autowired
    private ConfigurationService configurationService;

	@Test
	public void constructReconstructAndSaveDriver() {
		final Object testingData[][] = {
			{
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text1", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text2", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text3", null
			}, {
				null, "Text4", ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.constructReconstructAndSaveTemplate((String) testingData[i][0], ((String) testingData[i][1]), (Class<?>) testingData[i][2]);
	}

	// Ancillary methods ------------------------------------------------------

	protected void constructReconstructAndSaveTemplate(final String banner, final String name, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			
			ConfigurationForm configurationForm = configurationService.construct();
			configurationForm.setName(name);
			configurationForm.setBanner(banner);
			Configuration configuration = configurationService.reconstruct(configurationForm);
			configurationService.save(configuration);
			
			ConfigurationForm configurationForm2 = configurationService.construct();
			Assert.isTrue(configurationForm2.getName().equals(name));
			
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
