package services;

import domain.Category;
import forms.CategoryForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CategoryTestService extends AbstractTest {

	// TEST DE LOS CASOS DE USOS RELACIONADOS CON CATEGORY, CORRESPONDIENTES A
	// ACME RENDEZVOUS

	// PARA QUE ESTOS TEST FUNCIONEN CORRECTAMENTE LA BASE DE DATOS TIENE QUE
	// ESTAR EN UN ESTADO MUY ESPECIFICO, RECOMENDAMOS QUE SE POPULE ANTES DE
	// EJECUTARLO
	// NO GARANTIZAMOS EL CORRECTO FUNCIONAMIENTO DE LOS TEST SI SE HA
	// INTERACTUADO CON EL SISTEMA PREVIAMENTE

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ServiceService serviceService;

	// TEMPLATES ----------------------------------------------------
	// Test referente a la creaci�n de una categor�a hija. Con esto probamos la
	// creaci�n de categor�as padre e hijas.
	// Adem�s probamos el findOne
	// getChildrens

	public void createCategoryTestConPadre(final Integer categoryId,
			final Class<?> expected) {
		Class<?> caught = null;
		try {
			Category catPadre = categoryService.findOne(categoryId);
			final Category cFather = categoryService.create();
			cFather.setDescription(catPadre.getDescription());
			cFather.setName(catPadre.getName());
			Category cFatherSaved = this.categoryService.flush(cFather);
			final Category cSon = this.categoryService.create(cFatherSaved
					.getId());
			cSon.setFather(cFatherSaved);
			cSon.setDescription(catPadre.getDescription());
			cSon.setName(catPadre.getName());
			Category cSonSaved = this.categoryService.flush(cSon);

			Assert.notNull(this.categoryService.findOne(cSonSaved.getId()));

			Assert.notEmpty(this.categoryService.getChildrens(cFatherSaved
					.getId()));

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Test reconstruct
	protected void constructReconstructAndSaveTemplate(
			final String description, final String name, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {

			CategoryForm categoryForm = new CategoryForm();
			categoryForm.setName(name);
			categoryForm.setDescription(description);
			Category category = categoryService.reonstruct(categoryForm);
			Category cSonSaved = this.categoryService.save(category);

			Assert.notNull(this.categoryService.findOne(cSonSaved.getId()));

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Test referente a la creaci�n de categor�as padre.
	public void createCategoryTest(final Class<?> expected) {
		Class<?> caught = null;
		try {
			final Category c = this.categoryService.create();
			final Category cSaved = this.categoryService.save(c);
			Assert.notNull(this.categoryService.findOne(cSaved.getId()));
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Test referente al borrado de categorias.
	public void deleteCategoryTest(final Integer categoryId,
			final Class<?> expected) {
		Class<?> caught = null;
		try {
			this.categoryService.delete(this.categoryService
					.findOne(categoryId));
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.unauthenticate();
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateCategoryConPadre() {


		final Object testingData[][] = {{64, null},
				{0, IllegalArgumentException.class}};

		for (int i = 0; i < testingData.length; i++)
			this.createCategoryTestConPadre((Integer) testingData[i][0],
					((Class<?>) testingData[i][1]));
	}

	@Test
	public void driverDeleteCategory() {
		final Object testingData[][] = { { 64, null },
				{ 1, IllegalArgumentException.class },
				{ null, IllegalArgumentException.class } };
		for (int i = 0; i < testingData.length; i++)
			this.deleteCategoryTest((Integer) testingData[i][0],
					((Class<?>) testingData[i][1]));
	}

	@Test
	public void constructReconstructDriver() {
		final Object testingData[][] = { { "asdfgh", "qwert", null },

		{ null, "Name4", null },

		{ null, null, null },

		{ "Name4", null, null } };

		for (int i = 0; i < testingData.length; i++)
			this.constructReconstructAndSaveTemplate(
					(String) testingData[i][0], ((String) testingData[i][1]),
					(Class<?>) testingData[i][2]);
	}

}
