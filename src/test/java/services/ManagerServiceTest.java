package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Manager;
import forms.RegisterManagerForm;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ManagerServiceTest extends AbstractTest {

	@Autowired
	private ManagerService managerService;

	@Test
	public void saveAndDeleteDriver() {

		final Object testingData[][] = {
			{
				"name1", "surname1", "email1@email.com", "666666666", "direccion1","ATU00000169","password1","password1", false ,null
			},{
				"name2", "surname2", "email1@email.com", "666666666", "direccion1","ATU00000179","password2","password2", false,null
			}
		};
			
			
		for (int i = 0; i< testingData.length; i++){
			this.ReconstructSaveAndDeleteTemplate((String) testingData[i][0], 
			(String) testingData[i][1],(String) testingData[i][2], (String) testingData[i][3],
			(String) testingData[i][4], (String) testingData[i][5],(String) testingData[i][6],
			(String) testingData[i][7],(Boolean) testingData[i][8],(Class<?>) testingData[i][9]);
		}	
			
		

	}

	 protected void ReconstructSaveAndDeleteTemplate(final String name, final String surname,
			 final String email, final String phonenumber, final String direction,final String VAT,
			 final String password, final String repeatPassword,final boolean check, final Class<?> expected) { 
    Class<?> caught; 
 
    caught = null; 
    try { 
      
      RegisterManagerForm managerForm = new RegisterManagerForm(); 
      managerForm.setName(name);
      managerForm.setSurname(surname);
      managerForm.setAddress(direction);
      managerForm.setPhone(phonenumber);
      managerForm.setEmail(email);
      managerForm.setVAT(VAT);
      managerForm.setPassword(password);
      managerForm.setRepeatPassword(repeatPassword);
      managerForm.setCheck(check);
     
       
      Manager manager = managerService.reconstruct(managerForm); 
       
      manager = managerService.flush(manager);
      managerService.delete(manager); 
      Assert.isTrue(managerService.findOne(manager.getId())==null);
       
    } catch (final Throwable oops) { 
      caught = oops.getClass(); 
    } 
 
    this.checkExceptions(expected, caught); 
  } 
	
}
