package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import utilities.AbstractTest;
import domain.Category;
import domain.Manager;
import domain.Service;
import forms.ServiceForm;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ServiceServiceTest extends AbstractTest {

	// TEST DE LOS CASOS DE USOS RELACIONADOS CON Service, CORRESPONDIENTES A
	// ACME Rendezvous

	// PARA QUE ESTOS TEST FUNCIONEN CORRECTAMENTE LA BASE DE DATOS TIENE QUE
	// ESTAR EN UN ESTADO MUY ESPECIFICO, RECOMENDAMOS QUE SE POPULE ANTES DE
	// EJECUTARLO
	// NO GARANTIZAMOS EL CORRECTO FUNCIONAMIENTO DE LOS TEST SI SE HA
	// INTERACTUADO CON EL SISTEMA PREVIAMENTE

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ManagerService managerService;

	// TEMPLATES ----------------------------------------------------

	// En este test se inclute el save, el delete, el findOne, el Search y el
	// findByManger



	protected void constructReconstructAndSaveTemplate(final String name,
			final String description, final String picture,
			final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate("manager1");
			ServiceForm serviceForm = new ServiceForm();
			serviceForm.setName(name);
			serviceForm.setDescription(description);
			serviceForm.setPicture(picture);
			serviceForm.setCategory(categoryService.findOne(64));
			Service service = serviceService.reconstruct(serviceForm);

			Service sSaved = this.serviceService.flush(service);

			Assert.notNull(this.serviceService.findOne(sSaved.getId()));
			unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}


	@Test
	public void constructReconstructDriver() {
		final Object testingData[][] = { { "asdfgh", "qwert", "", null } };

		for (int i = 0; i < testingData.length; i++)
			this.constructReconstructAndSaveTemplate(
					(String) testingData[i][0], ((String) testingData[i][1]),
					((String) testingData[i][2]), (Class<?>) testingData[i][3]);
	}

}
