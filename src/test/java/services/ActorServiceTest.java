package services;


import domain.Actor;
import forms.ActorForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ActorServiceTest extends AbstractTest {

	@Autowired
	private ActorService actorService;

	@Test
	public void saveAndDeleteDriver() {

		final Object testingData[][] = {
			{
				//Caso Positivo
				"name1", "surname1", "email1@email.com", "666666666", "direccion1", null
			},{
				//Caso negativo: Pasamos el nombre a Null
				null, "surname4",  "email1@email.com", "666666666", "direccion1", DataIntegrityViolationException.class
			}
		};
			
			
		for (int i = 0; i< testingData.length; i++){
			this.reconstructSaveAndDeleteTemplate((String) testingData[i][0], 
			(String) testingData[i][1],(String) testingData[i][2], (String) testingData[i][3],
			(String) testingData[i][4], (Class<?>) testingData[i][5]); 
		}	
			
		

	}
	//Test para probar la creacion, edicion, guardado y borrado de un Actor.
	protected void reconstructSaveAndDeleteTemplate(final String name, final String surname, final String email, final String phonenumber, final String direction, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate("user1");
			ActorForm actorForm = new ActorForm();
			actorForm.setName(name);
			actorForm.setSurname(surname);
			actorForm.setDirection(direction);
			actorForm.setPhoneNumber(phonenumber);
			actorForm.setEmail(email);


			Actor actor = actorService.save(actorForm);
			actor = actorService.flush(actor);


			actorService.delete(actor);
			Assert.isNull(actorService.findOne(actor.getId()));
			unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	

	



}
