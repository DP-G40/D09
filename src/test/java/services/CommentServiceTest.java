/*
 * SampleTest.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Comment;
import forms.CommentForm;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CommentServiceTest extends AbstractTest {


	
	@Autowired
    private CommentService commentService;
	
	@Autowired
    private RendezvousService rendezvousService;
	
	@Autowired
    private UserService userService;

	@Test
	public void craateAndSaveDriver() {
		final Object testingData[][] = {
			{
				//Caso Positivo
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text1", null
			},  {
				//Caso negativo: Pasamos null en los campos
				null, null, ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.createAndSaveTemplate((String) testingData[i][0], ((String) testingData[i][1]), (Class<?>) testingData[i][2]);
	}

	//Test para probar la creacion de Comentarios
	protected void createAndSaveTemplate(final String picture, final String text, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate("user1");
			Comment comment = commentService.create(rendezvousService.findAll().iterator().next().getId());
			comment.setMoment(new Date());
			comment.setPicture(picture);
			comment.setText(text);
			Comment commentSaved = commentService.saveAndFlush(comment);
			Assert.isTrue(commentSaved.getId()!=0);
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	
	@Test
	public void reconstructAndDeleteDriver() {
		final Object testingData[][] = {
			{
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text1", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text2", null
			}, {
				"https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_582,c_limit/phonepicutres-TA.jpg", "Text3", null
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.reconstructAndDeleteTemplate((String) testingData[i][0], ((String) testingData[i][1]), (Class<?>) testingData[i][2]);
	}

	// Ancillary methods ------------------------------------------------------

	//Test para probar la edicion y borrado de comentarios
	protected void reconstructAndDeleteTemplate(final String picture, final String text, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate("user1");
			CommentForm commentForm = new CommentForm();
			commentForm.setPicture(picture);
			commentForm.setRendezvousId(rendezvousService.findAll().iterator().next().getId());
			commentForm.setText(text);
			Comment comment = commentService.reconstruct(commentForm);
			Comment commentSaved = commentService.save(comment);
			List<Comment> comments = new ArrayList<Comment>();
			comments.add(commentSaved);
			commentService.deleteAll(comments);
			Assert.isTrue(commentService.findOne(commentSaved.getId())==null);
			this.unauthenticate();
			
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}