package controllers;

import domain.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ServiceService;

import java.util.Collection;

@Controller
@RequestMapping("/service")
public class ServiceController extends AbstractController{

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("service/list");
        try{
            Collection<Service> services = serviceService.findAll();
            res.addObject("services",services);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/error/sorry.do");
        }
        return res;
    }
}
