package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CategoryService;
import services.ServiceService;
import controllers.AbstractController;
import domain.Actor;
import domain.Category;
import domain.Manager;
import forms.ServiceForm;

@Controller
@RequestMapping("/service/manager")
public class ServiceManagerController extends AbstractController {
	
	@Autowired
	private ServiceService serviceService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ActorService actorService;
	

	
	@RequestMapping(value = "/listOwn", method = RequestMethod.GET)
	public ModelAndView listOwn() {
		ModelAndView result;
		try {
			
			Manager manager = (Manager) actorService.getPrincipal();
			Integer managerId = manager.getId();
			Collection<domain.Service> services = serviceService.findByManager(managerId);
			result = new ModelAndView("service/list");
			result.addObject("services", services);
			result.addObject("requestURI", "/service/list.do");
			
		}catch (Throwable oops){
			result = new ModelAndView("redirect:/error/sorry.do");
		}
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res = new ModelAndView("service/create");
		Collection<Category> categories = categoryService.findAll();
		ServiceForm serviceForm = new ServiceForm();
		res.addObject("action", "service/manager/create.do");
		res.addObject("serviceForm", serviceForm);
		res.addObject("categories", categories);
		return res;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid ServiceForm serviceForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("service/create");
		Collection<Category> categories = categoryService.findAll();

		if (binding.hasErrors()) {
			res.addObject("action", "service/manager/create.do");
			res.addObject("serviceForm", serviceForm);
			res.addObject("categories", categories);
		} else {
			try {
				Manager m = (Manager) actorService.getPrincipal();
				domain.Service f = serviceService.reconstruct(serviceForm);
				f.setManager(m);
				
				f = serviceService.save(f);
				res = new ModelAndView("redirect:/service/manager/list.do");

			} catch (Throwable oops) {
				res.addObject("action", "service/manager/create.do");
				res.addObject("serviceForm", serviceForm);
				res.addObject("categories", categories);
				res.addObject("message", "actor.commit.error");

			}

		}
		return res;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int serviceId) {
		ModelAndView res = new ModelAndView("service/edit");
		domain.Service aux = serviceService.findOne(serviceId);
		Actor principal = actorService.getPrincipal();
		Collection<Category> categories = categoryService.findAll();


		if (aux.getManager().getId() != principal.getId()) {
			res = new ModelAndView("redirect:/error/sorry.do");
		} else {
			ServiceForm serviceForm = serviceService.construct(aux);
			res.addObject("action", "service/manager/create.do");
			res.addObject("serviceForm", serviceForm);
			res.addObject("categories", categories);
		}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEditPublish(@Valid ServiceForm serviceForm, BindingResult binding) {
		ModelAndView res = new ModelAndView("service/edit");
		Collection<Category> categories = categoryService.findAll();

		if (binding.hasErrors()) {
			res.addObject("action", "service/manager/create.do");
			res.addObject("serviceForm", serviceForm);
			res.addObject("categories", categories);
		} else {
			try {
				Manager u = (Manager) actorService.getPrincipal();
				domain.Service f = serviceService.reconstruct(serviceForm);
				f.setManager(u);
				
				f = serviceService.save(f);
				res = new ModelAndView("redirect:/service/manager/list.do");

			} catch (Throwable oops) {
				res.addObject("action", "service/manager/create.do");
				res.addObject("serviceForm", serviceForm);
				res.addObject("categories", categories);
				res.addObject("message", "actor.commit.error");

			}
		}
		return res;
	}
	
	 @RequestMapping(value = "/delete", method = RequestMethod.GET)
	    public ModelAndView delete(Integer serviceId) {
	        ModelAndView result;
	        try {

	            result = new ModelAndView(
	                    "redirect:/service/manager/list.do");	                            
	            serviceService.delete(serviceService.findOne(serviceId));

	        } catch (Throwable oops) {
	            result = new ModelAndView("redirect:/#");
	        }
	        return result;
	    }
	
}
