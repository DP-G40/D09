package controllers.user;

import controllers.AbstractController;
import domain.CreditCard;
import domain.Rendezvous;
import domain.Request;
import domain.Service;
import forms.RequestForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.RendezvousService;
import services.RequestService;
import services.ServiceService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/request/user")
public class RequestUserController extends AbstractController {

    @Autowired
    private RequestService requestService;
    @Autowired
    private ServiceService serviceService;
    @Autowired
    private RendezvousService rendezvousService;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create(@CookieValue(value = "brand", defaultValue = "") String brand,@CookieValue(value = "holder", defaultValue = "") String holder,@CookieValue(value = "number", defaultValue = "") String number,@CookieValue(value = "cvv", defaultValue = "") String cvv,@CookieValue(value = "month", defaultValue = "") String month,@CookieValue(value = "year", defaultValue = "") String year ) {
        ModelAndView res = new ModelAndView("request/create");
        try {
            RequestForm requestForm = new RequestForm();
            Collection<Rendezvous> rendezvous = rendezvousService.findMyRendezvous();
            Collection<Service> services = serviceService.findAvailable();
            if (StringUtils.isNotBlank(brand)) {
                CreditCard creditCard = new CreditCard(brand, holder, number, cvv, month, year);
                requestForm.setCreditCard(creditCard);
            }
            res.addObject("requestForm", requestForm);
            res.addObject("rendezvouses", rendezvous);
            res.addObject("services", services);
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid RequestForm requestForm, BindingResult result, HttpServletResponse response) {
        ModelAndView res = new ModelAndView("request/create");

        Collection<Rendezvous> rendezvous = rendezvousService.findMyRendezvous();
        Collection<Service> services = serviceService.findAvailable();
        res.addObject("rendezvouses", rendezvous);
        res.addObject("services", services);

        if (result.hasErrors()) {
            res.addObject("requestForm", requestForm);
        } else {
            try {
                Request request = requestService.reconstruct(requestForm);
                requestService.save(request);
                res = new ModelAndView("redirect:/rendezvous/user/view.do?rendezvousId=" + request.getRendezvous().getId());
                CreditCard creditCard = request.getCreditCard();
                Cookie brand = new Cookie("brand",creditCard.getBrand());
                Cookie holder = new Cookie("holder",creditCard.getHolderName());
                Cookie number = new Cookie("number",creditCard.getNumber());
                Cookie cvv = new Cookie("cvv",creditCard.getCvv().toString());
                Cookie month = new Cookie("month",creditCard.getExpiryMonth().toString());
                Cookie year = new Cookie("year",creditCard.getExpiryYear().toString());
                response.addCookie(brand);
                response.addCookie(holder);
                response.addCookie(number);
                response.addCookie(cvv);
                response.addCookie(month);
                response.addCookie(year);
            } catch (Throwable oops) {
                res.addObject("requestForm", requestForm);
                if (oops.getMessage().equalsIgnoreCase("creditcard.date.error")) {
                    res.addObject("message", "creditcard.date.error");
                } else if (oops.getMessage().equalsIgnoreCase("request.service.error")) {
                    res.addObject("message", "request.service.error");

                } else {
                    res.addObject("message", "category.commit.error");
                }

            }
        }
        return res;
    }


}
