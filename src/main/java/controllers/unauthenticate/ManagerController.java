package controllers.unauthenticate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ManagerService;

import controllers.AbstractController;
import domain.Manager;
import forms.RegisterManagerForm;

@Controller
@RequestMapping("/manager")
public class ManagerController extends AbstractController{
	
	@Autowired
	private ManagerService managerService;

	@RequestMapping(value = "register", method = RequestMethod.GET)
	public ModelAndView register() {
		ModelAndView res = new ModelAndView("manager/register");
		RegisterManagerForm registerManagerForm = new RegisterManagerForm();
		res.addObject("registerManagerForm", registerManagerForm);
		return res;

	}

	@RequestMapping(value = "register", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid RegisterManagerForm registerManagerForm,
			BindingResult result) {
		ModelAndView res = new ModelAndView("manager/register");
		res.addObject("registerManagerForm", registerManagerForm);
		if (!result.hasErrors()) {
			try {
				Manager manager= managerService.reconstruct(registerManagerForm);
				managerService.save(manager);
				res = new ModelAndView("redirect:/security/login.do");
			} catch (Throwable oops) {
				if (oops.getMessage().equals("register.error.password")) {
					res.addObject("message", "register.error.password");
				} else if (oops.getMessage().equals("register.error.username")) {
					res.addObject("message", "register.error.username");
				}
			}

		}
		return res;
	}
	
}
