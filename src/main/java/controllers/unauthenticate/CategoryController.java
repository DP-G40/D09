package controllers.unauthenticate;

import controllers.AbstractController;
import domain.Category;
import domain.Rendezvous;
import domain.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CategoryService;
import services.RendezvousService;
import services.ServiceService;


import java.util.Collection;

@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {


    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private RendezvousService rendezvousService;

    @RequestMapping(value="/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res = new ModelAndView("category/list");

       Category aux2 = categoryService.findAllByFatherMaster();
       Collection<Category> aux = categoryService.getChildrens(aux2.getId());

       Collection<Rendezvous> rendezvous = rendezvousService.findByCategory(aux2);

       Collection<domain.Service> trips = serviceService.searchByCategory(aux2);

        res.addObject("categories", aux);
        res.addObject("nombrecat", aux2.getName());
        res.addObject("service", trips);
        res.addObject("rendezvous", rendezvous);
        res.addObject("requestURI", "/category/list.do");


        return res;
    }

    @RequestMapping(value="/listchildren", method = RequestMethod.GET)
    public ModelAndView listChildrens(@RequestParam int categoryId) {
        ModelAndView res = new ModelAndView("category/list");
        try {
            Collection<Category> aux = categoryService.getChildrens(categoryId);
            Category c = categoryService.findOne(categoryId);
            Collection<domain.Service> trips = serviceService.searchByCategory(c);

            Collection<Rendezvous> rendezvous = rendezvousService.findByCategoryDraft(c);

            res.addObject("categories", aux);
            res.addObject("nombrecat", c.getName());
            res.addObject("service", trips);
            res.addObject("rendezvous", rendezvous);
            res.addObject("requestURI", "/category/list.do");

        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }


        return res;
    }
}
