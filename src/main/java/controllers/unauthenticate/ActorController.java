package controllers.unauthenticate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import controllers.AbstractController;
import domain.Actor;
import forms.ActorForm;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {

	@Autowired
	private ActorService actorService;

	public ActorController() {
		super();
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid ActorForm actorform, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = new ModelAndView("actor/edit");
			res.addObject("actorform", actorform);
		} else {
			try {
				actorService.save(actorform);
				res = new ModelAndView("redirect:/#");
			} catch (Throwable oops) {
				res = new ModelAndView("actor/edit");
				res.addObject("actorform", actorform);
				res.addObject("message", "actor.commit.error");
			}
		}

		return res;
	}

	protected ModelAndView createRegisterModelAndView(Actor actor,
			String messageCode) {
		ModelAndView res;

		res = new ModelAndView("actor/edit");
		res.addObject("actor", actor);
		res.addObject("message", messageCode);
		return res;

	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		Actor actor;

		actor = actorService.getPrincipal();
		ActorForm actorform = new ActorForm();
		actorform.setDirection(actor.getDirection());
		actorform.setEmail(actor.getEmail());
		actorform.setName(actor.getName());
		actorform.setPhoneNumber(actor.getPhoneNumber());
		actorform.setSurname(actor.getSurname());

		result = new ModelAndView("actor/edit");
		result.addObject("action", "actor/edit.do");
		result.addObject("actorform", actorform);
		return result;
	}

}
