package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.UserService;
import controllers.AbstractController;
import domain.User;
import forms.RegisterForm;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<User> users = userService.findAll();
		result = new ModelAndView("user/list");
		result.addObject("user", users);
		result.addObject("requestURI", "/user/list.do");
		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam Integer UserId) {
		ModelAndView result;
		try {
			result = new ModelAndView("user/view");

			User user = userService.findOne(UserId);
			result.addObject("user", user);
			result.addObject("rendezvouses", user.getRendezvous());
		} catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}
		return result;
	}

	@RequestMapping(value = "register", method = RequestMethod.GET)
	public ModelAndView register() {
		ModelAndView res = new ModelAndView("user/register");
		RegisterForm registerForm = new RegisterForm();
		res.addObject("registerForm", registerForm);
		return res;

	}

	@RequestMapping(value = "register", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid RegisterForm registerForm,
			BindingResult result) {
		ModelAndView res = new ModelAndView("user/register");
		res.addObject("registerForm", registerForm);
		if (!result.hasErrors()) {
			try {
				User user = userService.reconstruct(registerForm);
				userService.save(user);
				res = new ModelAndView("redirect:/security/login.do");
			} catch (Throwable oops) {
				if (oops.getMessage().equals("register.error.password")) {
					res.addObject("message", "register.error.password");
				} else if (oops.getMessage().equals("register.error.username")) {
					res.addObject("message", "register.error.username");
				}
			}

		}
		return res;
	}
}
