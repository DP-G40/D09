package controllers;

import java.util.Collection;

import domain.Comment;
import domain.Service;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.RendezvousService;
import controllers.AbstractController;
import domain.Rendezvous;

@Controller
@RequestMapping("/rendezvous")
public class RendezvousController extends AbstractController {

	@Autowired
	private RendezvousService rendezvousService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		try {
			Collection<Rendezvous> rendezvous = rendezvousService.findAllNot18();
			result = new ModelAndView("rendezvous/list");
			result.addObject("rendezvous", rendezvous);
			result.addObject("requestURI", "/rendezvous/list.do");
		}catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}
		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam Integer rendezvousId) {
		ModelAndView result;
		try {
			result = new ModelAndView("rendezvous/view");

			Rendezvous rendezvous = rendezvousService.findOneNot18(rendezvousId);
			result.addObject("rendezvous", rendezvous);
			Collection<User> userreg = rendezvous.getUsers();
			result.addObject("usersapuntados", userreg);
			Collection<Comment> comments = rendezvous.getComments();
			result.addObject("comments", comments);
			Collection<Service> services = rendezvous.getServices();
			result.addObject("services",services);
		}catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}
		return result;
	}

}
