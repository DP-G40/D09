/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import domain.Manager;
import domain.Rendezvous;
import domain.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import services.AdministratorService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {


	@Autowired
	private AdministratorService administratorService;

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

	@RequestMapping(value = "dashboard")
	public ModelAndView dashoard(){
		ModelAndView res = new ModelAndView("administrator/dashboard");
		Object q1 = administratorService.Q1();
		Double q2 = administratorService.Q2();
		Object q3 = administratorService.Q3();
		Object q4 = administratorService.Q4();
		List<Rendezvous> q5 = administratorService.Q5();
		Object q6 = administratorService.Q6();
		List<Rendezvous> q7 = administratorService.Q7();
		List<Rendezvous> q8 = administratorService.Q8();


		res.addObject("Q1",q1);
		res.addObject("Q2",q2);
		res.addObject("Q3",q3);
		res.addObject("Q4",q4);
		res.addObject("Q5",q5);
		res.addObject("Q6",q6);
		res.addObject("Q7",q7);
		res.addObject("Q8",q8);
		
		//2.0
		
		List<Service> q9 = administratorService.Q9();
		List<Manager> q10= administratorService.Q10();
		List<Manager> q11 = administratorService.Q11();
		Double q12 = administratorService.Q12();
		Object q13 = administratorService.Q13();
		Object q14 = administratorService.Q14();
		List<Service> q15 = administratorService.Q15();
		
		res.addObject("Q9",q9);
		res.addObject("Q10",q10);
		res.addObject("Q11",q11);
		res.addObject("Q12",q12);
		res.addObject("Q13",q13);
		res.addObject("Q14",q14);
		res.addObject("Q15",q15);

		return res;
	}

}
