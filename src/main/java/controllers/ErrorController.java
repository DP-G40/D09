package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/error")
public class ErrorController extends AbstractController  {

    @RequestMapping("/sorry")
    public ModelAndView error() {
        ModelAndView result;

        result = new ModelAndView("error/sorry");

        return result;
    }
}
