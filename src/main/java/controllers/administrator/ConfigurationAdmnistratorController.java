package controllers.administrator;

import controllers.AbstractController;
import domain.Configuration;
import forms.ConfigurationForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ConfigurationService;

import javax.validation.Valid;

@Controller
@RequestMapping("/configuration/administrator")
public class ConfigurationAdmnistratorController extends AbstractController{

    @Autowired
    private ConfigurationService configurationService;

    @RequestMapping(value = "/configuration",method = RequestMethod.GET)
    public ModelAndView edit(){
        ModelAndView res = new ModelAndView("configuration/edit");
        try {
            Configuration conf = this.configurationService.find();
            ConfigurationForm configurationForm = this.configurationService.construct();
            res.addObject("configurationForm", configurationForm);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/configuration", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid ConfigurationForm configurationForm, BindingResult result) {
        ModelAndView res = new ModelAndView("configuration/edit");
        if (result.hasErrors()) {
            res.addObject("configurationForm", configurationForm);
        } else {
            try {
                Configuration conf = this.configurationService.reconstruct(configurationForm);
                this.configurationService.save(conf);
                res = new ModelAndView("redirect:/#");
            } catch (Throwable oops) {
                res = new ModelAndView("configuration/edit");
            }
        }
        return res;
    }
}
