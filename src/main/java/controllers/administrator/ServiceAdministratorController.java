package controllers.administrator;

import domain.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ServiceService;

@Controller
@RequestMapping("/service/administrator")
public class ServiceAdministratorController {

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "/cancel",method = RequestMethod.GET)
    public ModelAndView cancel(@RequestParam Integer serviceId){
        ModelAndView res = new ModelAndView("redirect: /service/list.do");
        try {
            Service service = serviceService.findOne(serviceId);
            service.setCancelled(true);
            serviceService.save(service);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/error/sorry.do");
        }
        return res;
    }
}
