package controllers.administrator;

import controllers.AbstractController;
import domain.Comment;
import domain.Rendezvous;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CommentService;
import services.RendezvousService;

import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/rendezvous/administrator")
public class RendezvousAdministratorController extends AbstractController{

    @Autowired
    private RendezvousService rendezvousService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView result;
        try {
            Collection<Rendezvous> rendezvous = rendezvousService.findAll();
            result = new ModelAndView("rendezvous/list");
            result.addObject("rendezvous", rendezvous);
            result.addObject("requestURI", "/rendezvous/list.do");
        }catch (Throwable oops){
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer rendezvousId) {
        ModelAndView result;
        try {
            result = new ModelAndView("rendezvous/view");
            Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
            result.addObject("rendezvous", rendezvous);
            Collection<User> userreg = rendezvous.getUsers();
            result.addObject("usersapuntados", userreg);
            Collection<Comment> comments = rendezvous.getComments();
            result.addObject("comments", comments);
        }catch (Throwable oops){
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView list(Integer rendezvousId) {
        ModelAndView result;
        Collection<Rendezvous> rendezvous = new ArrayList<>();
        try {

            commentService.deleteAll(rendezvousService.findOne(rendezvousId)
                    .getComments());

            rendezvousService.findOne(rendezvousId).setBanned(true);
            rendezvousService.findOne(rendezvousId).setActive(false);
            rendezvous = rendezvousService.findAll();
        } catch (Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        result = new ModelAndView("rendezvous/list");
        result.addObject("rendezvous", rendezvous);
        result.addObject("requestURI", "/rendezvous/list.do");
        return result;
    }
}
