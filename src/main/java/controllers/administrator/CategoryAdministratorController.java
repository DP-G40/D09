package controllers.administrator;

import controllers.AbstractController;
import domain.Category;
import forms.CategoryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CategoryService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/category/administrator")
public class CategoryAdministratorController extends AbstractController {

    //Los admin son los que controlan el arbol de categorias
    @Autowired
    private CategoryService categoryService;


    public CategoryAdministratorController() {
        super();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView res;
        try {
            Collection<Category> categories = categoryService.findAll();
            ;
            res = new ModelAndView("category/list");
            res.addObject("categories", categories);
            res.addObject("requestURI", "/category/administrator/list.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }


        return res;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView result = new ModelAndView("category/create");
        try {
            CategoryForm categoryForm = new CategoryForm();
            result.addObject("categoryForm", categoryForm);
            result.addObject("categories", categoryService.findAll());
            result.addObject("action", "category/administrator/create.do");
        } catch (Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam Integer categoryId) {
        ModelAndView res = new ModelAndView("category/edit");
        Category category;
        try {
            category = categoryService.findOne(categoryId);
            CategoryForm categoryForm = this.categoryService.construct(category);
            Collection<Category> categories = categoryService.findAll();
            categories.remove(category);
            res.addObject("categoryForm", categoryForm);
            res.addObject("categories", categories);
            res.addObject("action", "category/administrator/edit.do");
            if (categoryService.getChildrens(category.getId()).size() != 0){
                res.addObject("disabled", "true");
                res.addObject("message", "category.edit.childrens");
            }
        } catch (Throwable oops) {
            res = new ModelAndView("redirect:/#");
        }

        return res;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid CategoryForm categoryForm, BindingResult result) {
        ModelAndView res = new ModelAndView("category/edit");
        Collection<Category> categories = categoryService.findAll();
        Category thisCategory = this.categoryService.findOne(categoryForm.getCategoryId());
        categories.remove(thisCategory);
        res.addObject("categories",categories);

        if (result.hasErrors()) {
            res.addObject("categoryForm", categoryForm);
        } else {
            try {
                Category category = this.categoryService.reonstruct(categoryForm);
                categoryService.save(category);
                res = new ModelAndView("redirect:/category/administrator/list.do");

            } catch (Throwable oops) {
                res.addObject("message","category.commit.error");
            }
        }

        return res;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
    public ModelAndView saveCreate(@Valid CategoryForm categoryForm, BindingResult result) {
        ModelAndView res = new ModelAndView("category/create");
        if (result.hasErrors()) {
            res.addObject("categories", categoryService.findAll());
            res.addObject("categoryForm", categoryForm);
        } else {
            try {
                Category category = this.categoryService.reonstruct(categoryForm);
                categoryService.save(category);
                res = new ModelAndView("redirect:/category/administrator/list.do");

            } catch (Throwable oops) {
                res.addObject("categoryForm", categoryForm);
                res.addObject("categories", categoryService.findAll());
                res.addObject("message","category.commit.error");
            }
        }

        return res;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
    public ModelAndView delete(CategoryForm categoryForm, BindingResult binding) {
        ModelAndView res = new ModelAndView("category/edit");
        Collection<Category> categories = categoryService.findAll();
        Category thisCategory = this.categoryService.findOne(categoryForm.getCategoryId());
        categories.remove(thisCategory);
        res.addObject("categories",categories);
        try {
            Integer numChildrens = categoryService.getChildrens(thisCategory.getId()).size();
            if (numChildrens == 0) {
                Category category = categoryService.reonstruct(categoryForm);
                categoryService.delete(category);
                res = new ModelAndView("redirect:list.do");
            } else {
                res.addObject("categoryForm",categoryForm);
                res.addObject("message","category.commit.error.have.childrens");
            }
        } catch (Throwable oops) {
            res.addObject("categoryForm",categoryForm);
            res.addObject("message","category.commit.error");
        }

        return res;
    }


}
