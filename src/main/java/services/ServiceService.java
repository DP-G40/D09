package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.ServiceRepository;
import domain.Category;
import domain.Manager;
import forms.ServiceForm;

@Service
@Transactional
public class ServiceService {

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private ActorService actorService;

	public ServiceService() {
		super();
	}

	public domain.Service create() {
		domain.Service result;

		result = new domain.Service();
		Manager manager = (Manager) actorService.getPrincipal();
		result.setManager(manager);
		result.setCancelled(false);
		return result;
	}

	public Collection<domain.Service> findAll() {
		Collection<domain.Service> result;
		Assert.notNull(serviceRepository);
		result = serviceRepository.findAll();
		Assert.notNull(result);
		return result;
	}

	public domain.Service findOne(int serviceId) {
		domain.Service result;
		result = serviceRepository.findOne(serviceId);
		return result;
	}

	public domain.Service save(domain.Service service) {
		assert service != null;
		domain.Service result;
		result = serviceRepository.save(service);
		return result;
	}

	public void delete(domain.Service service) {
		assert service != null;
		assert service.getId() != 0;
		serviceRepository.delete(service);
	}

	public Collection<domain.Service> findByManager(Integer managerId) {
		return serviceRepository.findByManager(managerId);
	}

	public Collection<domain.Service> findAvailable() {
		return serviceRepository.findAvailable();
	}

	public ServiceForm construct(domain.Service service) {
		ServiceForm res = new ServiceForm();
		res.setName(service.getName());
		res.setDescription(service.getDescription());
		res.setPicture(service.getPicture());
		res.setCancelled(service.getCancelled());
		service.setCategory(service.getCategory());
		return res;
	}

	public domain.Service reconstruct(ServiceForm serviceForm) {
		domain.Service result;

		result = this.create();

		result.setName(serviceForm.getName());
		result.setDescription(serviceForm.getDescription());
		result.setPicture(serviceForm.getPicture());
		result.setCancelled(serviceForm.getCancelled());
		result.setCategory(serviceForm.getCategory());

		return result;

	}

	public Collection<domain.Service> searchByCategory(Category cat) {
		Collection<domain.Service> res = serviceRepository.searchByCategory(cat
				.getId());
		Assert.notNull(res);
		return res;
	}

	public domain.Service flush(domain.Service service){
		return serviceRepository.saveAndFlush(service);
	}

}
