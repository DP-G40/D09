package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ManagerRepository;
import security.Authority;
import security.UserAccount;
import domain.Manager;
import forms.RegisterManagerForm;

@Service
@Transactional
public class ManagerService {

	
	
	   @Autowired
	    private ManagerRepository managerRepository;
	   
	   @Autowired
	    private ActorService actorService;
	// Suporting repository --------------------------------------------------

	    // Constructors -----------------------------------------------------------
	    public ManagerService() {
	        super();
	    }

	    // Simple CRUD methods ----------------------------------------------------
	    public Manager create() {
	        Manager result;
	        result = new Manager();
	   
	        return result;
	    }

	    public Collection<Manager> findAll() {
	        Collection<Manager> result;
	        Assert.notNull(managerRepository);
	        result = managerRepository.findAll();
	        Assert.notNull(result);
	        return result;
	    }

	    public Manager findOne(int managerId) {
	        Manager result;
	        result = managerRepository.findOne(managerId);
	        return result;
	    }

	    public Manager save(Manager manager) {
	        assert manager != null;
	        Manager result;
	        result = managerRepository.save(manager);
	        return result;
	    }

	    public void delete(Manager manager) {
	        assert manager != null;
	        assert manager.getId() != 0;
	        managerRepository.delete(manager);
	    }
	    
	    
	 // Other business methods -------------------------------------------------

	    public Manager reconstruct(RegisterManagerForm registerManagerForm){
	        Manager res = this.create();
	        Assert.isTrue(registerManagerForm.getPassword().equals(registerManagerForm.getRepeatPassword()),"register.error.password");
	        Assert.isNull(actorService.getByUserName(registerManagerForm.getUserName()),"register.error.username");
	        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
	        UserAccount userAccount = new UserAccount();
	        Authority a = new Authority();
	        a.setAuthority(Authority.MANAGER);
	        userAccount.getAuthorities().add(a);
	        userAccount.setPassword(md5.encodePassword(registerManagerForm.getPassword(),null));
	        userAccount.setUsername(registerManagerForm.getUserName());
	        res.setUserAccount(userAccount);
	        res.setName(registerManagerForm.getName());
	        res.setSurname(registerManagerForm.getSurname());
	        res.setPhoneNumber(registerManagerForm.getPhone());
	        res.setEmail(registerManagerForm.getEmail());
	        res.setDirection(registerManagerForm.getAddress());
	        res.setVAT(registerManagerForm.getVAT());
	      

	        return res;

	    }

	    public Manager flush(Manager manager){
	    	return managerRepository.saveAndFlush(manager);
		}
	    
}
