package services;

import domain.Category;

import forms.CategoryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.CategoryRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ServiceService serviceService;


    public CategoryService(){
        super();
    }

    public Category create(){
        Category result;

        result = new Category();

        return result;
    }

    public Category create(Integer categoryId){
        Category result;

        result = new Category();
        Category father = this.findOne(categoryId);
        result.setFather(father);

        return result;
    }


    public Collection<Category> findAll(){
        Collection<Category> result;

        result = categoryRepository.findAll();

        Assert.notNull(result);

        return result;
    }

    public Category findAllByFatherMaster(){
        Category result;

        result = categoryRepository.findAllByFatherMaster();

        Assert.notNull(result);

        return result;
    }



    public Category findOne(final Integer categoryId){
        Category result;

        Assert.notNull(categoryId);

        result = categoryRepository.findOne(categoryId);

        Assert.notNull(result);

        return result;


    }

    public Category save(final Category category){
        Category result;

        Assert.notNull(category);

        result = categoryRepository.save(category);

        Assert.notNull(result);

        return result;

    }

    public void delete(final Category category){
        Assert.notNull(category);
        for(domain.Service t : serviceService.searchByCategory(category)){
            t.setCategory(category.getFather());
            serviceService.save(t);
        }
        category.setFather(null);
        categoryRepository.delete(category);




    }

    public List<Category> getChildrens(int categoryId) {
        return categoryRepository.getChildrens(categoryId);
    }

    public List<Category> getChildrens(int categoryId, String name) {
        return categoryRepository.getChildrens(categoryId, name);
    }

    public CategoryForm construct(Category category){
        CategoryForm res = new CategoryForm();
        res.setDescription(category.getDescription());
        res.setName(category.getName());
        res.setFather(category.getFather());
        res.setCategoryId(category.getId());
        return res;
    }

    public Category reonstruct(CategoryForm categoryForm){
        Category res;
        if (categoryForm.getCategoryId() == 0){
            res = this.create();
        } else {
            res = this.findOne(categoryForm.getCategoryId());
        }
        res.setDescription(categoryForm.getName());
        res.setName(categoryForm.getName());
        res.setFather(categoryForm.getFather());
        return res;
    }

    public Category flush(Category category){
        return categoryRepository.saveAndFlush(category);
    }






}
