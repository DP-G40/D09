package services;

import domain.Rendezvous;
import domain.Request;
import domain.User;
import forms.RequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RequestRepository;

import java.util.Calendar;

@Service
@Transactional
public class RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RendezvousService rendezvousService;

    @Autowired
    private ActorService actorService;

    public Request create(){
        Request res = new Request();
        res.setUser((User) actorService.getPrincipal());
        Assert.notNull(res);
        return res;
    }

    public Request save(Request request){
        Assert.notNull(request);
        Request res = requestRepository.save(request);
        Assert.notNull(res);
        Rendezvous rendezvous = request.getRendezvous();
        rendezvous.getServices().add(request.getService());
        rendezvousService.save(rendezvous);
        return res;
    }

    public Request reconstruct(RequestForm requestForm){
        Request res = this.create();
        res.setComments(requestForm.getComments());
        res.setRendezvous(requestForm.getRendezvous());
        res.setService(requestForm.getService());
        res.setCreditCard(requestForm.getCreditCard());
        Calendar time = Calendar.getInstance();
        Integer mes = time.get(Calendar.MONTH) + 1 ;
        Integer anio = time.get(Calendar.YEAR);
        Assert.isTrue((requestForm.getCreditCard().getExpiryYear().equals(anio) && (requestForm.getCreditCard().getExpiryMonth().compareTo(mes) >= 1) ) || (requestForm.getCreditCard().getExpiryYear() > anio),"creditcard.date.error");
        Assert.isTrue(!requestForm.getRendezvous().getServices().contains(requestForm.getService()),"request.service.error");
        return res;
    }
}
