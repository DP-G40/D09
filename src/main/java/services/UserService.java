package services;

import domain.User;
import forms.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.UserRepository;
import security.Authority;
import security.UserAccount;

import java.util.Collection;

@Service
@Transactional
public class UserService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ActorService actorService;

// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public UserService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public User create() {
        User result;
        result = new User();
        return result;
    }

    public Collection<User> findAll() {
        Collection<User> result;
        Assert.notNull(userRepository);
        result = userRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public User findOne(int userId) {
        User result;
        result = userRepository.findOne(userId);
        return result;
    }

    public User save(User user) {
        assert user != null;
        User result;
        result = userRepository.save(user);
        return result;
    }

    public void delete(User user) {
        assert user != null;
        assert user.getId() != 0;
        userRepository.delete(user);
    }

// Other business methods -------------------------------------------------

    public User reconstruct(RegisterForm registerForm){
        User res = this.create();
        Assert.isTrue(registerForm.getPassword().equals(registerForm.getRepeatPassword()),"register.error.password");
        Assert.isNull(actorService.getByUserName(registerForm.getUserName()),"register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        UserAccount userAccount = new UserAccount();
        Authority a = new Authority();
        a.setAuthority(Authority.USER);
        userAccount.getAuthorities().add(a);
        userAccount.setPassword(md5.encodePassword(registerForm.getPassword(),null));
        userAccount.setUsername(registerForm.getUserName());
        res.setUserAccount(userAccount);
        res.setName(registerForm.getName());
        res.setSurname(registerForm.getSurname());
        res.setPhoneNumber(registerForm.getPhone());
        res.setEmail(registerForm.getEmail());
        res.setDirection(registerForm.getAddress());
        res.setBirthday(registerForm.getBirthday());

        return res;

    }

    public User flush(User user){
        return userRepository.saveAndFlush(user);
    }
}
