package services;

import domain.Configuration;
import forms.ConfigurationForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ConfigurationRepository;

import java.util.Collection;

@Service
@Transactional
public class ConfigurationService {

    @Autowired
    private ConfigurationRepository configurationRepository;

    public Configuration find(){
        Configuration res = this.configurationRepository.findAll().iterator().next();
        Assert.notNull(res);
        return res;
    }

    public Configuration save(Configuration configuration){
        Assert.notNull(configuration);
        Configuration res = this.configurationRepository.save(configuration);
        Assert.notNull(res);
        return res;
    }

    public Configuration reconstruct(ConfigurationForm configurationForm){
        Configuration res = this.find();
        res.setBanner(configurationForm.getBanner());
        res.setName(configurationForm.getName());
        res.setWelcome(configurationForm.getMessage());
        res.setWelcome_es(configurationForm.getMessageES());
        return res;
    }

    public ConfigurationForm construct(){
        ConfigurationForm res = new ConfigurationForm();
        Configuration configuration = this.find();
        res.setBanner(configuration.getBanner());
        res.setName(configuration.getName());
        res.setMessage(configuration.getWelcome());
        res.setMessageES(configuration.getWelcome_es());
        return res;
    }
}
