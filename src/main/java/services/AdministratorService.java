package services;

import domain.Administrator;
import domain.Manager;
import domain.Rendezvous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;

import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class AdministratorService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private AdministratorRepository administratorRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public AdministratorService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Administrator create() {
        Administrator result;
        result = new Administrator();
        return result;
    }

    public Collection<Administrator> findAll() {
        Collection<Administrator> result;
        Assert.notNull(administratorRepository);
        result = administratorRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Administrator findOne(int administratorId) {
        Administrator result;
        result = administratorRepository.findOne(administratorId);
        return result;
    }

    public Administrator save(Administrator administrator) {
        assert administrator != null;
        Administrator result;
        result = administratorRepository.save(administrator);
        return result;
    }

    public void delete(Administrator administrator) {
        assert administrator != null;
        assert administrator.getId() != 0;
        administratorRepository.delete(administrator);
    }

    public Object Q1() {
        return administratorRepository.Q1();
    }

    public Double Q2() {
        return administratorRepository.Q2();
    }

    public Object Q3() {
        return administratorRepository.Q3();
    }

    public Object Q4() {
        return administratorRepository.Q4();
    }

    public List<Rendezvous> Q5() {
        return administratorRepository.Q5(new PageRequest(0, 10));
    }

    public Object Q6() {
        return administratorRepository.Q6();
    }

    public List<Rendezvous> Q7() {
        return administratorRepository.Q7();
    }

    public List<Rendezvous> Q8() {
        return administratorRepository.Q8();
    }

    //2.0
    
	public List<Manager> Q10() {
		return administratorRepository.Q10();
	}

	public List<Manager> Q11() {
		return administratorRepository.Q11();
	}

	public Object Q14() {
		return administratorRepository.Q14();
	}

	public List<domain.Service> Q15() {
		return administratorRepository.Q15(new PageRequest(0, 10));
	}

	public List<domain.Service> Q9() {
		return administratorRepository.Q9();
	}

	public Double Q12() {
		return administratorRepository.Q12();
	}

	public Object Q13() {
		return administratorRepository.Q13();
	}
    
// Other business methods -------------------------------------------------


}
