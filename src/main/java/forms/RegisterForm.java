package forms;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class RegisterForm {

    // Attributes
    private String userName;
    private String password;
    private String repeatPassword;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String address;
    private Boolean check = false;
    private Date birthday;



    //Getters and Setters
    @Size(min = 5, max = 32)
    @NotBlank
    @NotNull
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Size(min = 5, max = 32)
    @NotBlank
    @NotNull
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Size(min = 5, max = 32)
    @NotBlank
    @NotNull
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getRepeatPassword() {
        return repeatPassword;
    }
    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    @NotBlank
    @NotNull
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @NotBlank
    @NotNull
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Email
    @NotNull
    @NotBlank
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }


    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }


    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }


    @AssertTrue
    public Boolean getCheck() {
        return check;
    }
    public void setCheck(Boolean check) {
        this.check = check;
    }

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
