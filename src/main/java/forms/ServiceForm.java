package forms;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import domain.Category;
import domain.Manager;

public class ServiceForm {

	
	  private String name;

	    private String description;

	    private String picture;

	    private Boolean cancelled = false;

	    @NotBlank
	    @NotNull
	    @SafeHtml(whitelistType = WhiteListType.NONE)
	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    @NotBlank
	    @NotNull
	    @SafeHtml(whitelistType = WhiteListType.NONE)
	    public String getDescription() {
	        return description;
	    }

	    public void setDescription(String description) {
	        this.description = description;
	    }

	    @URL
	    @SafeHtml(whitelistType = WhiteListType.NONE)
	    public String getPicture() {
	        return picture;
	    }

	    public void setPicture(String picture) {
	        this.picture = picture;
	    }

	    @NotNull
	    public Boolean getCancelled() {
	        return cancelled;
	    }

	    public void setCancelled(Boolean cancelled) {
	        this.cancelled = cancelled;
	    }
	    
	    private Category category;

	    @NotNull
	    public Category getCategory() {
	        return category;
	    }

	    public void setCategory(Category category) {
	        this.category = category;
	    }
}
