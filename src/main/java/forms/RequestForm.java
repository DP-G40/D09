package forms;

import domain.CreditCard;
import domain.Rendezvous;
import domain.Service;
import domain.User;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class RequestForm {

    private CreditCard creditCard;

    private String comments;

    private Service service;

    private Rendezvous rendezvous;

    @NotNull
    @Valid
    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @NotNull
    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @NotNull
    public Rendezvous getRendezvous() {
        return rendezvous;
    }

    public void setRendezvous(Rendezvous rendezvous) {
        this.rendezvous = rendezvous;
    }
}
