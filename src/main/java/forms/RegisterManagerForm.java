package forms;

import java.util.Date;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

public class RegisterManagerForm {

	  // Attributes
    private String userName;
    private String password;
    private String repeatPassword;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String address;
    private Boolean check = false;
    private String VAT;



    //Getters and Setters
    @Size(min = 5, max = 32)
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Size(min = 5, max = 32)
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Size(min = 5, max = 32)
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getRepeatPassword() {
        return repeatPassword;
    }
    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    @NotBlank
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @NotBlank
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Email
    @NotNull
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }


    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }


    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }


    @AssertTrue
    public Boolean getCheck() {
        return check;
    }
    public void setCheck(Boolean check) {
        this.check = check;
    }
    
    @SafeHtml(whitelistType = WhiteListType.NONE)
    public String getVAT(){
    	return VAT;
    }
    public void setVAT(String vat){
    	this.VAT = vat;
    }

  
}
