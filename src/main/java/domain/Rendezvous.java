package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {@Index(columnList = "draft"),@Index(columnList = "active"),@Index(columnList = "banned"),@Index(columnList = "adultflag"),@Index(columnList = "author_id")})
public class Rendezvous extends DomainEntity {

    // Atributos ----
    private String name;
    private String description;
    private Date moment;
    private String picture;
    private Double coordinateX;
    private Double coordinateY;
    private Boolean active = true;
    private Boolean draft = true;
    private Boolean banned = false;
    private Boolean adultflag = false;

    // Constructor ----

    public Rendezvous() {
        super();
    }

    @NotNull
    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    @NotBlank
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.DATE)
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getMoment() {
        return moment;
    }

    public void setMoment(Date moment) {
        this.moment = moment;
    }

    @URL
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public Double getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(Double coordinateX) {
        this.coordinateX = coordinateX;
    }


    public Double getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(Double coordinateY) {
        this.coordinateY = coordinateY;
    }

    @NotNull
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


    public Boolean getDraft() {
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    @NotNull
    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    @NotNull
    public Boolean getAdultflag() {
        return adultflag;
    }

    public void setAdultflag(Boolean adultflag) {
        this.adultflag = adultflag;
    }

    private User author;
    private Collection<User> users = new ArrayList<User>();
    private Collection<Rendezvous> rendezvous = new ArrayList<Rendezvous>();
    private Collection<Comment> comments = new ArrayList<Comment>();
    private Collection<Announcement> announcements = new ArrayList<Announcement>();
    private Collection<Service> services = new ArrayList<Service>();
    private Collection<Request> requests = new ArrayList<Request>();

    @NotNull
    @ManyToOne
    public User getAuthor() {
        return author;
    }

    public void setAuthor(User user) {
        this.author = user;
    }

    @NotNull
    @ManyToMany
    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }


    @NotNull
    @OneToMany()
    public Collection<Rendezvous> getRendezvous() {
        return rendezvous;
    }

    public void setRendezvous(Collection<Rendezvous> rendezvous) {
        this.rendezvous = rendezvous;
    }

    @OneToMany(mappedBy = "rendezvous")
    @NotNull
    public Collection<Comment> getComments() {
        return comments;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }

    @OneToMany(mappedBy = "rendezvous")
    public Collection<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(Collection<Announcement> announcements) {
        this.announcements = announcements;
    }

    @ManyToMany
    @NotNull
    public Collection<Service> getServices() {
        return services;
    }

    public void setServices(Collection<Service> services) {
        this.services = services;
    }

    @OneToMany(mappedBy = "rendezvous")
	public Collection<Request> getRequests() {
		return requests;
	}
	public void setRequests(Collection<Request> requests) {
		this.requests = requests;
	}
}