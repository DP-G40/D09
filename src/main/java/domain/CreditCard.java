package domain;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Embeddable
@Access(AccessType.PROPERTY)
public class CreditCard {

    // Atributos ----

    private String	holderName;
    private String	brand;
    private String	number;
    private Integer	expiryMonth;
    private Integer	expiryYear;
    private Integer cvv;


    // Constructor ----

    public CreditCard() {
        super();
    }

    public CreditCard(String brand,String holder,String number,String cvv,String month,String year){
        this.brand = brand;
        this.holderName = holder;
        this.number = number;
        this.cvv = Integer.valueOf(cvv);
        this.expiryMonth = Integer.valueOf(month);
        this.expiryYear = Integer.valueOf(year);

    }

    @NotNull
    @NotBlank
    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holdername) {
        this.holderName = holdername;
    }

    @NotNull
    @NotBlank
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @NotNull
    @NotBlank
    @CreditCardNumber
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @NotNull
    @Range(min = 1,max = 12)
    public Integer getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(Integer expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    @NotNull
    @Min(2017)
    public Integer getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(Integer expiryYear) {
        this.expiryYear = expiryYear;
    }

    @NotNull
    @Range(min = 100,max=999)
    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }
}
