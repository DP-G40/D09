package repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Administrator;
import domain.Manager;
import domain.Rendezvous;
import domain.Service;
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	//C
		@Query("select avg(u.rendezvous.size),stddev(u.rendezvous.size) from User u")
		Object Q1();
		
		@Query("select sum(case when u.rendezvous.size > 0 then 1.00 else 0.00 end)/sum(case when u.rendezvous.size = 0 then 1.00 else 0.00 end) from User u")
		Double Q2();
		
		@Query("select avg(r.users.size),stddev(r.users.size) from Rendezvous r")
		Object Q3();
		
		@Query("select avg(u.rendezvous.size),stddev(u.rendezvous.size) from User u join u.rendezvous r where r.users.size >= 1")
		Object Q4();
		
		@Query("select r from Rendezvous r order by r.users.size")
		List<Rendezvous> Q5(Pageable pageable);
		
		//B
		
		@Query("select avg(r.announcements.size),stddev(r.announcements.size) from Rendezvous r")
		Object Q6();
		
		@Query("select r from Rendezvous r where r.announcements.size >= ALL(select avg(r2.announcements.size)*0.75 from Rendezvous r2)")
		List<Rendezvous> Q7();
		
		@Query("select r from Rendezvous r where r.rendezvous.size >= ALL(select avg(r2.rendezvous.size)*1.10 from Rendezvous r2)")
		List<Rendezvous> Q8();
		
		// 2.0
		
		//C
		
		@Query("select r.service from Request r group by r.service.id having count(r) >= ALL(select count(r) from Request r group by r.service.id)")
		List<Service> Q9();
		
		@Query("select m from Manager m where m.services.size >= ALL(select avg(m.services.size) from Manager m)")
		List<Manager> Q10();
		
		@Query("select m from Manager m join m.services s where s.cancelled=false and m.services.size >= ALL(select count(s) from Manager m join m.services s where s.cancelled=false)")
		List<Manager> Q11();
		
		//B
		
		@Query("select avg(r.services.size) from Rendezvous r") // un servicio tiene una categoria asociada y no puede ser nulo
		Double Q12();
		
		@Query("select (count(s)*1.0)/(count(r)*1.0),s.category.name from Rendezvous r join r.services s group by s.category.id")
		List<Object> Q13();
		
		@Query("select avg(r.requests.size),min(r.requests.size),max(r.requests.size),stddev(r.requests.size) from Rendezvous r")
		Object Q14();
		
		@Query("select r.service from Request r group by r.service.id order by count(r) desc")
		List<Service> Q15(Pageable pageable);

}

