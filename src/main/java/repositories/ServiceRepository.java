package repositories;

import java.util.Collection;

import domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Rendezvous;
import domain.Service;
import services.ServiceService;

@Repository
public interface ServiceRepository extends JpaRepository<domain.Service, Integer>{

	  @Query("select s from Service s where  s.manager.id = ?1")
	  Collection<domain.Service> findByManager(Integer managerId);
	  
	  @Query("select s from Service s where s.cancelled = false")
	  Collection<domain.Service> findAvailable();

	  @Query("select t from Service t where t.category.id = ?1")
	  Collection<domain.Service> searchByCategory(int cat);
}
