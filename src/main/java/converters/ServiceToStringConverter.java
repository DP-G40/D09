package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class ServiceToStringConverter implements
		Converter<domain.Service, String> {

	public String convert(domain.Service service) {
		String result;
		if (service == null)
			result = null;
		else
			result = String.valueOf(service.getId());
		return result;
	}

}
