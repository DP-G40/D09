<%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>



<form:form action="manager/register.do" modelAttribute="registerManagerForm" method="post">

	<acme:textbox code="manager.name" path="name" />
	<acme:textbox code="manager.surname" path="surname" />
	<acme:textbox code="manager.email" path="email" />
	<acme:textbox code="manager.phone" path="phone" />
	<acme:textbox code="manager.address" path="address" />
	<acme:textbox code="manager.VAT" path="VAT" />
	<br />
	<acme:textbox code="manager.userName" path="userName" />
	<acme:password code="manager.password" path="password" />
	<acme:password code="manager.repeatPassword" path="repeatPassword" />
	<acme:checkbox code="manager.termsAndConditions" path="check" />
	<br />

	<br />
	
	<acme:submit name="save" code="manager.save"/>
	<acme:cancel code="manager.cancel" url="" />

</form:form>