<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<div class="centrado">
    <div class="query">
        <spring:message code="dashboard.Q1"/>:<br><br>
        </h2> <jstl:out value="${Q1[0]}"/> - </h2> <jstl:out value="${Q1[1]}"/><br>
    </div><br><br>
    <div class="query">
        <spring:message code="dashboard.Q2"/>:<br><br>
        </h2> <jstl:out value="${Q2}"/><br>
    </div><br>  <br>
    <div class="query">
        <spring:message code="dashboard.Q3"/>:<br><br>
        </h2> <jstl:out value="${Q3[0]}"/> - </h2> <jstl:out value="${Q3[1]}"/><br>
    </div><br><br>
    <div class="query">
        <spring:message code="dashboard.Q4"/>:<br><br>
        </h2> <jstl:out value="${Q4[0]}"/> - </h2> <jstl:out value="${Q4[1]}"/><br>
    </div><br><br>
    <div class="query">
        <spring:message code="dashboard.Q5"/>:
    </div>
    <display:table name="Q5" id="row" >
        <display:column property="name" titleKey="rendezvous.name" sortable="true" class="todos"/>
        <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                        class="todos"/>
    </display:table><br>
    <div class="query">
        <spring:message code="dashboard.Q6"/>:<br><br>
        </h2> <jstl:out value="${Q6[0]}"/> - </h2> <jstl:out value="${Q4[1]}"/><br>
    </div><br><br>
    <div class="query">

        <spring:message code="dashboard.Q7"/>:
    </div>
    <display:table name="Q7" id="row" >
        <display:column property="name" titleKey="rendezvous.name" sortable="true" class="todos"/>
        <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                        class="todos"/>
    </display:table><br>
    <div class="query">
        <spring:message code="dashboard.Q8"/>:
    </div>
    <display:table name="Q8" id="row" >
        <display:column property="name" titleKey="rendezvous.name" sortable="true" class="todos"/>
        <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                        class="todos"/>
    </display:table><br>
    
    <div class="query">
        <spring:message code="dashboard.Q9"/>:
    </div>
    <display:table name="Q9" id="row" >
        <display:column property="name" titleKey="service.name" sortable="true" class="todos"/>
    </display:table><br>
    
    <div class="query">
        <spring:message code="dashboard.Q10"/>:
    </div>
    <display:table name="Q10" id="row" >
        <display:column property="name" titleKey="manager.name" sortable="true" class="todos"/>
    </display:table><br>
    
    <div class="query">
        <spring:message code="dashboard.Q11"/>:
    </div>
    <display:table name="Q11" id="row" >
        <display:column property="name" titleKey="manager.name" sortable="true" class="todos"/>
    </display:table><br>
    
    <div class="query">
        <spring:message code="dashboard.Q12"/>:<br><br>
        </h2> <jstl:out value="${Q12}"/><br>
    </div><br>  <br>
    
    <div class="query">
        <spring:message code="dashboard.Q13"/>:<br><br>
        </h2> <jstl:out value="${Q13[1][0]}"/> - </h2> <jstl:out value="${Q13[0][0]}"/><br>
    </div><br><br>
    
    <div class="query">
        <spring:message code="dashboard.Q14"/>:<br><br>
        <jstl:out value="${Q14[0]}"/> -  <jstl:out value="${Q14[1]}"/> - <jstl:out value="${Q14[2]}"/> - <jstl:out value="${Q14[3]}"/><br>
    </div><br><br>
    
     <div class="query">
        <spring:message code="dashboard.Q15"/>:
    </div>
    <display:table name="Q15" id="row" >
        <display:column property="name" titleKey="service.name" sortable="true" class="todos"/>
    </display:table><br>
</div>