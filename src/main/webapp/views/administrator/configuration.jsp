<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>



<form:form action="configuration/administrator/configuration.do" modelAttribute="configurationForm">


    <acme:textbox code="configuration.name" path="name" />
    <acme:textbox code="configuration.banner" path="banner" />
    <acme:textbox code="configuration.message" path="message" />
    <acme:textbox code="configuration.messageES" path="messageES" />

    <tr>
        <td colspan="3">
            <acme:submit code="announcement.save" name="save" />
            <acme:cancel code="announcement.cancel" url="/#" />

        </td>
    </tr>

</form:form>
