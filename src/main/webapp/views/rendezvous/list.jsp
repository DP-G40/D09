<%@page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>
<%
    Date date = new Date();
    pageContext.setAttribute("now", date);
    Integer mes = 60 * 60 * 24 * 30 * 1000;
%>


<h2><spring:message code="user.rendezvouses"/></h2>
<display:table name="rendezvous" id="row" requestURI="${requestURI}">

    <jstl:choose>
        <jstl:when test="${row.banned==true}">


            <display:column property="name" titleKey="rendezvous.name" sortable="true" class="banned"/>
            <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                            class="banned"/>

            <display:column titleKey="user.view" class="banned">
                <acme:cancel code="rendezvous.creator" url="/user/view.do?UserId=${row.author.id}"/>
            </display:column>

            <security:authorize access="isAnonymous()">
                <display:column titleKey="user.view" class="banned">
                    <acme:cancel code="user.view" url="rendezvous/view.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('USER')">
                <display:column titleKey="user.view" class="banned">
                    <acme:cancel code="user.view" url="rendezvous/user/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="rendezvous.rvsp" class="pasado">
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <display:column titleKey="user.view" class="banned">
                    <acme:cancel code="user.view" url="rendezvous/administrator/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="standard.delete" class="banned">
                </display:column>
            </security:authorize>
        </jstl:when>


        <jstl:when test="${row.moment.before(now)}">


            <display:column property="name" titleKey="rendezvous.name" sortable="true" class="pasado"/>
            <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                            class="pasado"/>

            <display:column titleKey="rendezvous.creator" class="pasado">
                <acme:cancel code="rendezvous.creator" url="/user/view.do?UserId=${row.author.id}"/>
            </display:column>

            <security:authorize access="isAnonymous()">
                <display:column titleKey="user.view" class="pasado">
                    <acme:cancel code="user.view" url="rendezvous/view.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('USER')">
                <display:column titleKey="user.view" class="pasado">
                    <acme:cancel code="user.view" url="rendezvous/user/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="rendezvous.rvsp" class="pasado">
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <display:column titleKey="user.view" class="pasado">
                    <acme:cancel code="user.view" url="rendezvous/administrator/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="standard.delete" class="pasado">
                    <acme:cancel code="standard.delete" url="rendezvous/administrator/delete.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

        </jstl:when>

        <jstl:when test="${row.draft==true}">


            <display:column property="name" titleKey="rendezvous.name" sortable="true" class="draft"/>
            <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                            class="draft"/>

            <display:column titleKey="rendezvous.creator" class="draft">
                <acme:cancel code="rendezvous.creator" url="/user/view.do?UserId=${row.author.id}"/>
            </display:column>

            <security:authorize access="isAnonymous()">
                <display:column titleKey="user.view" class="draft">
                    <acme:cancel code="user.view" url="rendezvous/view.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('USER')">
                <display:column titleKey="user.view" class="draft">
                    <acme:cancel code="user.view" url="rendezvous/user/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="rendezvous.rvsp" class="draft">
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <display:column titleKey="user.view" class="draft">
                    <acme:cancel code="user.view" url="rendezvous/administrator/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="standard.delete" class="draft">
                    <acme:cancel code="standard.delete" url="rendezvous/administrator/delete.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

        </jstl:when>

        <jstl:when test="${row.adultflag==true}">


            <display:column property="name" titleKey="rendezvous.name" sortable="true" class="adultflag"/>
            <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                            class="adultflag"/>


            <display:column titleKey="rendezvous.creator" class="adultflag">
                <acme:cancel code="rendezvous.creator" url="/user/view.do?UserId=${row.author.id}"/>
            </display:column>

            <security:authorize access="isAnonymous()">
                <display:column titleKey="user.view" class="adultflag">
                    <acme:cancel code="user.view" url="rendezvous/view.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('USER')">
                <display:column titleKey="user.view" class="adultflag">
                    <acme:cancel code="user.view" url="rendezvous/user/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="rendezvous.rvsp" class="adultflag">
                    <c:if test="${!row.users.contains(actor)}">
                        <acme:cancel code="rendezvous.rvsp" url="rendezvous/user/rsvp.do?rendezvousId=${row.id}"/>
                    </c:if>
                    <c:if test="${row.users.contains(actor)}">
                        <acme:cancel code="rendezvous.cancel" url="rendezvous/user/cancel.do?rendezvousId=${row.id}"/>
                    </c:if>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <display:column titleKey="user.view" class="adultflag">
                    <acme:cancel code="user.view" url="rendezvous/administrator/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="standard.delete" class="adultflag">
                    <acme:cancel code="standard.delete" url="rendezvous/administrator/delete.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

        </jstl:when>

        <jstl:otherwise>
            <display:column property="name" titleKey="rendezvous.name" sortable="true" class="todos"/>
            <display:column property="moment" titleKey="rendezvous.moment" sortable="false" format="{0,date,dd/MM/yyyy}"
                            class="todos"/>


            <display:column titleKey="rendezvous.creator" class="todos">
                <acme:cancel code="rendezvous.creator" url="/user/view.do?UserId=${row.author.id}"/>
            </display:column>

            <security:authorize access="isAnonymous()">
                <display:column titleKey="user.view" class="todos">
                    <acme:cancel code="user.view" url="rendezvous/view.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('USER')">
                <display:column titleKey="user.view" class="todos">
                    <acme:cancel code="user.view" url="rendezvous/user/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="rendezvous.rvsp" class="todos">
                    <c:if test="${!row.users.contains(actor)}">
                        <acme:cancel code="rendezvous.rvsp" url="rendezvous/user/rsvp.do?rendezvousId=${row.id}"/>
                    </c:if>
                    <c:if test="${row.users.contains(actor)}">
                        <acme:cancel code="rendezvous.cancel" url="rendezvous/user/cancel.do?rendezvousId=${row.id}"/>
                    </c:if>
                </display:column>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <display:column titleKey="user.view" class="todos">
                    <acme:cancel code="user.view" url="rendezvous/administrator/view.do?rendezvousId=${row.id}"/>
                </display:column>
                <display:column titleKey="standard.delete" class="todos">
                    <acme:cancel code="standard.delete" url="rendezvous/administrator/delete.do?rendezvousId=${row.id}"/>
                </display:column>
            </security:authorize>
        </jstl:otherwise>

    </jstl:choose>
</display:table>





<h3><spring:message code="rendezvous.legacy"/></h3>

<table class="tableLegacy">
  <thead>
    <tr>
      <th class="banned" scope="col"><spring:message code="rendezvous.banned"/></th>
      <th class="pasado" scope="col"><spring:message code="rendezvous.pasado"/></th>
      <th class="draft" scope="col"><spring:message code="rendezvous.draft"/></th>
      <th class="adultflag" scope="col"><spring:message code="rendezvous.adultflag"/></th>
      <th class="todos" scope="col"><spring:message code="rendezvous.other"/></th>
    </tr>
   
</table>



