<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<security:authentication property="principal" var="principal"/>

<jstl:if test="${rendezvous.picture != null}">
    <img src="${rendezvous.picture}" style="width: 30%; height:auto;">
</jstl:if>

<div class="table">
    <table class="rendezvous_ver">

        <tr>
            <td><b><spring:message code="rendezvous.name"></spring:message></b>: ${rendezvous.name}</td>
        </tr>


        <tr >
            <td><b><spring:message code="rendezvous.description"></spring:message></b>: ${rendezvous.description}</td>
            <td><b><spring:message code="rendezvous.moment"></spring:message></b>: ${rendezvous.moment}</td>

        </tr>
        <tr>
            <td><b><spring:message code="rendezvous.creator"></spring:message></b>: ${rendezvous.author.name} ${rendezvous.author.surname}</td>
        </tr>







    </table>

    <table class="rendezvous_ver_more">
        <tr >
            <td><b><spring:message code="rendezvous.coordinateX"></spring:message></b>: ${rendezvous.coordinateX}</td>
            <td><b><spring:message code="rendezvous.coordinateY"></spring:message></b>: ${rendezvous.coordinateY}</td>
        </tr>
    </table>

</div>




<h2><spring:message code="rendezvous.users"></spring:message></h2>
<display:table name="usersapuntados" id="row" requestURI="${requestURI}">
    <display:column property="name" titleKey="user.name" sortable="true"/>
    <display:column property="surname" titleKey="user.surname"/>

    <display:column titleKey="user.view">
        <acme:cancel code="user.view" url="/user/view.do?UserId=${row.id}"/>
    </display:column>


</display:table>

<h2><spring:message code="rendezvous.services"></spring:message></h2>
<display:table name="services" id="row" requestURI="${requestURI}">
    <display:column property="name" titleKey="user.name" sortable="true"/>
    <display:column property="description" titleKey="service.description"/>
</display:table>

<h2><spring:message code="rendezvous.comments"></spring:message></h2>
<display:table name="comments" id="row" requestURI="${requestURI}">
    <display:column property="text" titleKey="comments.text" sortable="true"/>
    <display:column property="user.name" titleKey="comments.text" sortable="true"/>
    <display:column titleKey="comments.picture">
    <img src="${row.picture}" style="width: 30%; height:auto;">
    </display:column>
    <security:authorize access="hasRole('ADMIN')">
        <display:column titleKey="standard.delete">
            <acme:cancel code="standard.delete" url="comment/administrator/delete.do?commentId=${row.id}"></acme:cancel>
        </display:column>
    </security:authorize>
</display:table>
</br>
</br>
<security:authorize access="hasRole('USER')">
    <c:if test="${rendezvous.users.contains(actor)}">
        <acme:cancel code="rendezvous.comment" url="comment/user/create.do?rendezvousId=${rendezvous.id}"></acme:cancel>
    </c:if>
</security:authorize>
</br>
</br>
<jstl:if test="${rendezvous.draft == true && rendezvous.banned == false && rendezvous.author.userAccount.id == principal.id}">
    <div class="buttons_actions_rendezvous">
        <acme:cancel code="standard.edit" url="rendezvous/user/edit.do?rendezvousId=${rendezvous.id}"></acme:cancel>

    </div>
</jstl:if>



<security:authorize access="isAuthenticated()">

    <acme:cancel url="rendezvous/user/list.do" code="rendz.back"/>

</security:authorize>

<security:authorize access="!isAuthenticated()">

    <acme:cancel url="rendezvous/list.do" code="rendz.back"/>

</security:authorize>


