<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="request/user/create.do" modelAttribute="requestForm">


   <acme:select path="service" code="request.service" items="${services}" itemLabel="name"/>
    <acme:select path="rendezvous" code="request.rendezvous" items="${rendezvouses}" itemLabel="name"/>
    <acme:textbox code="row.holderName" path="creditCard.holderName"/>
    <acme:textbox code="row.brand" path="creditCard.brand"/>
    <acme:textbox code="row.number" path="creditCard.number"/>
    <acme:textbox code="row.month" path="creditCard.expiryMonth" type="number" min="1" max="12" step="1"/>
    <acme:textbox code="row.year" path="creditCard.expiryYear" type="number" min="2018" max="9999" step="1"/>
    <acme:textbox code="row.cvv" path="creditCard.cvv" type="number" min="100" max="999" step="1"/>


    <acme:textarea code="request.comments" path="comments" />





    <tr>
        <td colspan="3">
            <acme:submit code="service.save" name="save" />
            <acme:cancel code="service.cancel" url="/#" />

        </td>
    </tr>

</form:form>
