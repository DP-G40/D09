<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8" %>

<meta charset="UTF-8">

<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>


<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div>
	<a href=""><img class="img-responsive center-block" src=${banner }
		style="margin-bottom: -10px; margin-top: -25px;" /></a>
</div>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="">${name}</a>
        </div>

        <ul class="nav navbar-nav">



            <security:authorize access="isAnonymous()">
                <li><a href="user/list.do"><spring:message
                        code="master.page.listUser"/></a></li>
                <li><a href="rendezvous/list.do"><spring:message
                        code="master.page.listRendezvous"/></a></li>
                <li class="dropdown"><a href="category/list.do"><spring:message code="master.page.category.list"/></a></li>

            </security:authorize>

            <security:authorize access="hasRole('USER')">
            <li><a href="user/list.do"><spring:message
                    code="master.page.listUser"/></a></li>
            <li><a href="rendezvous/user/list.do"><spring:message
                    code="rendezvous.listAll"/></a></li>
            <li class="dropdown"><a href="category/list.do"><spring:message code="master.page.category.list"/></a>                </li>
            <li><a href="request/user/create.do"><spring:message
                        code="request.create"/></a></li>
                <li class="dropdown"><a href="" class="dropdown-toggle"
                                        data-toggle="dropdown"><spring:message
                        code="rendezvous.MyRendezvous"/><span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        <li><a href="rendezvous/user/myrendezvous.do"><spring:message
                                code="rendezvous.listMyRendezvous"/></a></li>

                        <li><a href="rendezvous/user/create.do"><spring:message
                                code="rendezvous.user.create"/></a></li>

                    </ul>
                </li>

            </security:authorize>
            <security:authorize access="hasRole('MANAGER')">
                <li><a href="user/list.do"><spring:message
                        code="master.page.listUser"/></a></li>
                <li><a href="rendezvous/list.do"><spring:message
                        code="master.page.listRendezvous"/></a></li>
            </security:authorize>



            <security:authorize access="hasRole('ADMIN')">
                <li><a href="user/list.do"><spring:message
                        code="master.page.listUser"/></a></li>
                <li><a href="rendezvous/administrator/list.do"><spring:message
                        code="master.page.listRendezvous"/></a></li>
                <li><a href="administrator/dashboard.do"><spring:message
                        code="master.page.dashboard"/></a></li>
                <li><a href="configuration/administrator/configuration.do"><spring:message
                        code="master.page.configuration"/></a></li>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href=""><spring:message
                        code="master.page.category.list"/> <span class="caret"></span></a>
                    <security:authorize access="hasRole('ADMIN')">
                        <ul class="dropdown-menu">
                            <li><a href="category/administrator/list.do"><spring:message
                                    code="master.page.administrator.category"/></a></li>
                        </ul>
                    </security:authorize>
                </li>
            </security:authorize>









        </ul>

        <ul class="nav navbar-nav navbar-right">
            <security:authorize access="isAuthenticated()">
                <li class="dropdown"><a href="" class="dropdown-toggle"
                                        data-toggle="dropdown"> <spring:message
                        code="master.page.profile"/> (<security:authentication
                        property="principal.username"/>) <span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        <li><a href="j_spring_security_logout"><spring:message
                                code="master.page.logout"/> </a></li>
                    </ul>
                </li>
            </security:authorize>
            <security:authorize access="isAnonymous()">
                <li><a href="security/login.do"><spring:message
                        code="master.page.login"/></a></li>

                <li><a href="user/register.do"><spring:message
                    code="master.page.register.user"/></a></li>
            <li><a href="manager/register.do"><spring:message
                    code="master.page.register.manager"/></a></li>
            </security:authorize>
            <li><a href="?language=en"><img class="flag"
                                            src="https://lipis.github.io/flag-icon-css/flags/4x3/us.svg"
                                            style="width: 24px;" alt="United States of America Flag"></a></li>
            <li><a href="?language=es"><img class="flag"
                                            src="https://lipis.github.io/flag-icon-css/flags/4x3/es.svg"
                                            style="width: 24px;" alt="Spain Flag"></a></li>
        </ul>


    </div>
</nav>
<br>