<%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${action}" modelAttribute="categoryForm" method="post">

	<form:hidden path="categoryId"/>
	
	<acme:textbox path="name" code="category.name"/>
	<acme:textbox path="description" code="rendezvous.description"/>
	<acme:select path="father" code="category.father" items="${categories}" itemLabel="name"/>
	<br>
	
	<acme:submit name="save" code="actor.save"/>


	
	<acme:cancel code="category.cancel" url="/category/administrator/list.do"/>
	
</form:form>
