<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%
	Date date = new Date();
	SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
	String now = sp.format(date);
	pageContext.setAttribute("now",now);
%>
<br><br>
<form:form action="user/register.do" modelAttribute="registerForm" method="post">
<div class="row">
	<div class="col-xs-12 form-group">
		<acme:textbox code="user.name" path="name" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="user.surname" path="surname" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="user.email" path="email" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="user.phone" path="phone" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="user.address" path="address" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="user.birthday" path="birthday" type="date" max="${now}"/>
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="user.userName" path="userName" />
	</div><div class="col-xs-12 form-group">
		<acme:password code="user.password" path="password" />
	</div><div class="col-xs-12 form-group">
		<acme:password code="user.repeatPassword" path="repeatPassword" />
	</div>
	
	<br><br>
	<div class="col-xs-12 form-group">
		<acme:checkbox code="user.termsAndConditions" path="check" /><br><br>
		<acme:submit name="save" code="user.save"/>
		<acme:cancel code="user.cancel" url="" />
	</div>
</div>
</form:form>
