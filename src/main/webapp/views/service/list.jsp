<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>



<display:table name="service" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">
	
	<display:column property="name" titleKey="service.name" sortable="false"/>
	
	<display:column property="descriprion" titleKey="service.description" sortable="false" />
	
	<display:column property="picture" titleKey="service.picture" sortable="false"/>
	
	<display:column property="category" titleKey="service.category" sortable="false"/>
	
	
	<security:authorize access="hasRole('MANAGER')">
	<display:column>
		<acme:cancel code="service.delete"
			url="/service/manager/delete.do" />
	</display:column>
	</security:authorize>

</display:table>


<security:authorize access="hasRole('MANAGER')">

    <acme:cancel url="service/manager/create.do" code="service.create"/>

</security:authorize>



