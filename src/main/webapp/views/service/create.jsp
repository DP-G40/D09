<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${action}" modelAttribute="serviceForm">

	

	<acme:textbox code="service.name" path="name" />
	<acme:textbox code="service.description" path="description" />
	<acme:textbox code="service.picture" path="picture" />

	 <b><spring:message code="service.category" /></b>
    <form:select path="category">
        <form:options items="${categories}" itemLabel="name" itemValue="id"/>
    </form:select>
	

	<tr>
		<td colspan="3">
		<acme:submit code="service.save" name="save" /> 
		<acme:cancel code="service.cancel" url="/#" />
		
		</td>
	</tr>

</form:form>
