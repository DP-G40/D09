<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p align="justify">
	<strong><spring:message code="contact.title" /></strong> <br />
	<spring:message code="contact.description" />
	<br />
	<a href="mailto:ferferbor@alum.us.es">Ferm�n Fern�ndez Borrego</a>
	<br/>
	<a href="mailto:carsevbar@alum.us.es">Carlos Sevilla Barcel�</a>
	<br/>
	<a href="mailto:alesancor@alum.us.es">Alejandro Manuel Santana Cordero</a>
	<br/>
	<a href="mailto:frahidpla@alum.us.es">Francisco Hidalgo Plaza</a>
	<br/>
	<a href="mailto:juajimgui1@alum.us.es">Juan Ignacio Jimenez Dominguez</a>
	</p>