
<%--
 * login.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<link
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">


<form:form action="j_spring_security_check" modelAttribute="credentials">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-4 col-md-4">
				<div class="form-login">


					<form:label path="username">
						<spring:message code="security.username" />
					</form:label>
					<form:input class="form-control input-sm chat-input"
						path="username" />
					<form:errors class="error" path="username" />

					<br>

					<form:label path="password">
						<spring:message code="security.password" />
					</form:label>
					<form:password path="password"
						class="form-control input-sm chat-input" />
					<form:errors class="error" path="password" />
					<br>
					<jstl:if test="${showError == true}">
						<div class="error">
							<spring:message code="security.login.failed" />
						</div>
					</jstl:if>
					<br>
					<div class="wrapper">
						<span class="group-btn"> <input
							class="btn btn-primary btn-md" type="submit"
							value="<spring:message code="security.login" />" />

						</span>
					</div>
				</div>

			</div>
		</div>
	</div>
</form:form>


