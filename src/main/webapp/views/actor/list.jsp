<%@page import="com.lowagie.tools.plugins.AbstractTool.Console"%>
<%@ page import="java.util.Date" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>


    <h2><spring:message code="apply.accepted"/></h2>
    <display:table name="appliesAccepted" id="row1" requestURI="${requestURI}" class="accepted">
        <display:column property="trip.title" title="trip"/>
        <display:column property="moment" titleKey="apply.momment" sortable="true" format="{0,date,dd/MM/yyyy}"/>



        <display:column property="status" titleKey="apply.status"/>


        <display:column property="comments" titleKey="apply.comments" sortable="false"/>


        <display:column property="reason" titleKey="apply.reason" sortable="false"/>
        <security:authorize access="hasRole('EXPLORER')">
        <display:column>
            <jstl:if test="${row1.trip.begingDate.after(now)}">
                <acme:cancel code="apply.cancel" url="/apply/explorer/cancel.do?applyId=${row1.id}"/>
            </jstl:if>
        </display:column>
        </security:authorize>
    </display:table>

    <h2><spring:message code="apply.due"/></h2>
    <display:table name="appliesDue" id="row2" requestURI="${requestURI}" class="due">
        <display:column property="trip.title" title="trip"/>
        <display:column property="moment" titleKey="apply.momment" sortable="true" format="{0,date,dd/MM/yyyy}"/>


        <display:column property="status" titleKey="apply.status"/>


        <display:column property="comments" titleKey="apply.comments" sortable="false"/>


        <display:column property="reason" titleKey="apply.reason" sortable="false"/>
        <security:authorize access="hasRole('EXPLORER')">
        <display:column >
            <acme:cancel code="apply.creditcard" url="/apply/explorer/entercreditcard.do?applyId=${row2.id}"/>
        </display:column>
        </security:authorize>
    </display:table>

    <h2><spring:message code="apply.pendingWarning"/></h2>

    <display:table name="appliesPendinRed" id="row3" requestURI="${requestURI}" >  
    	
    	<display:column property="trip.title" class="pending2" title="trip"/>
        <display:column property="trip.begingDate" class="pending2" titleKey="apply.momment" sortable="true" format="{0,date,dd/MM/yyyy}"/>


        <display:column property="status" class="pending2" titleKey="apply.status"/>


        <display:column property="comments" class="pending2" titleKey="apply.comments" sortable="false"/>


        <display:column property="reason" class="pending2" titleKey="apply.reason" sortable="false"/>
      <security:authorize access="hasRole('MANAGER')">
            <display:column titleKey="apply.edit">
                <jstl:if test="${row3.status=='PENDING' &&  row3.trip.manager.userAccount.id==principal.id}">
                    <acme:cancel url="/apply/manager/due.do?applyId=${row3.id}" code="apply.due"/>

                    <acme:cancel url="/apply/manager/rejected.do?applyId=${row3.id}" code="apply.rejected"/>
                </jstl:if>
            </display:column>

        </security:authorize>
   </display:table> 
      
   <h2><spring:message code="apply.pending"/></h2>   
  <display:table name="appliesPendinWhite" id="row3" requestURI="${requestURI}" >  
    <display:column property="trip.title" class="pending" title="trip"/>
        <display:column property="trip.begingDate" class="pending" titleKey="apply.momment" sortable="true" format="{0,date,dd/MM/yyyy}"/>


        <display:column property="status" class="pending" titleKey="apply.status"/>


        <display:column property="comments" class="pending" titleKey="apply.comments" sortable="false"/>


        <display:column property="reason" class="pending" titleKey="apply.reason" sortable="false"/>
    
        <security:authorize access="hasRole('MANAGER')">
            <display:column titleKey="apply.edit">
                <jstl:if test="${row3.status=='PENDING' &&  row3.trip.manager.userAccount.id==principal.id}">
                    <acme:cancel url="/apply/manager/due.do?applyId=${row3.id}" code="apply.due"/>

                    <acme:cancel url="/apply/manager/rejected.do?applyId=${row3.id}" code="apply.rejected"/>
                </jstl:if>
            </display:column>

        </security:authorize>
       
    </display:table> 
        <br />


    <h2><spring:message code="apply.cancelled"/></h2>
    <display:table name="appliesCancelled" id="row4" requestURI="${requestURI}" class="cancelled">
        <display:column property="trip.title" title="trip" />
        <display:column property="moment" titleKey="apply.momment" sortable="true" format="{0,date,dd/MM/yyyy}" />


        <display:column property="status" titleKey="apply.status"/>


        <display:column property="comments" titleKey="apply.comments" sortable="false"/>


        <display:column property="reason" titleKey="apply.reason" sortable="false"/>
    </display:table>

    <h2><spring:message code="apply.rejected"/></h2>
    <display:table name="appliesRejected" id="row5" requestURI="${requestURI}" class="rejected">
        <display:column property="trip.title" title="trip" />
        <display:column property="moment" titleKey="apply.momment" sortable="true" format="{0,date,dd/MM/yyyy}" />


        <display:column property="status" titleKey="apply.status"/>


        <display:column property="comments" titleKey="apply.comments" sortable="false"/>


        <display:column property="reason" titleKey="apply.reason" sortable="false"/>
    </display:table>

	
	

	

